export const environment = {
    production: false,
    nodeUri: 'http://localhost:3333/api',
    apiUri: 'http://localhost:8000/api',
    apiLogout: 'http://localhost:8000/logout',
    googleSignInURL:'http://localhost:8000/auth/login',
    maxInactiveMin: 10
};
