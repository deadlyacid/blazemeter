export const environment = {
  production: true,
  apiUri: 'http://localhost:8000/api',
  apiLogout: 'http://localhost:8000/logout',
  googleSignInURL:'http://localhost:8000/auth/login',
  maxInactiveMin: 30
};
