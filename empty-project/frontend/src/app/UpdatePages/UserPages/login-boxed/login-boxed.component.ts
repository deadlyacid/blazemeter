import { Component, OnInit } from '@angular/core';
import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-login-boxed',
  templateUrl: './login-boxed.component.html',
  styleUrls: ['./login-boxed.css']
})
export class LoginBoxedComponent implements OnInit {
  loginWithGoogleURL: string = environment.googleSignInURL;
  reason: string = null;

  constructor() { }

  ngOnInit() {
      const reason = localStorage.getItem('reason');
      if(reason){
          this.reason = 'Your session has expired, Please login again.';
      }
  }

  resetReson(){
      localStorage.removeItem('reason');
      this.reason = null;
  }


}
