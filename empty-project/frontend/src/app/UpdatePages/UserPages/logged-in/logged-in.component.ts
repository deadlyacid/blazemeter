import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import { AuthenticationService } from "../../Services/authentication.service";
import {state} from "@angular/animations";

@Component({
  selector: 'app-logged-in',
  templateUrl: './logged-in.component.html',
  styleUrls: []
})
export class LoggedInComponent implements OnInit {

  constructor(private route: ActivatedRoute,
              private authSrv: AuthenticationService,
              private router: Router) { }

  ngOnInit() {
      this.route.paramMap.subscribe(params => {
        if (params.has('user')){
            const user = params.get('user');
            let res = this.authSrv.setLoggedIn(JSON.parse(atob(user)));
            this.router.navigate(['pages/update-status'], { queryParams: {  } });
        }
      });
  }

}
