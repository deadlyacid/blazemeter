export class User {
    id: number;
    name: string;
    email: string;
    fullName: string;
    avatar?: string;
    token?: string;
}