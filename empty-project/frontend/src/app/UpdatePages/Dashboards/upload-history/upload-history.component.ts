import { Component, OnInit } from '@angular/core';
import { UploadHistoryService } from "../../Services/upload-history-service.service";
import { Observable, of } from 'rxjs';
import * as _ from 'lodash';

@Component({
  selector: 'app-upload-history',
  templateUrl: './upload-history.component.html',
  styleUrls: []
})
export class UploadHistoryComponent implements OnInit {

  public uploadHistories$: Observable<any>;
  public _: any = _;

  constructor(private hu: UploadHistoryService) { }

  ngOnInit() {
      this.uploadHistories$ = this.hu.getUploadHistories();
  }

  getPage(page?: any) {
      this.uploadHistories$ = this.hu.getUploadHistories(page);
  }

  getGitUrl(version: string){
      if(version.indexOf('dirty') === -1){
          let versionArr = version.split('-');
          return `https://bitbucket.org/shield_meah/magen/commits/?search=${versionArr[versionArr.length-1]}`;
      }
      return null;
  }

}
