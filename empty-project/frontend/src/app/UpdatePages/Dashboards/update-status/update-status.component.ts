import { Component, OnInit } from '@angular/core';
import { UpdateService } from "../../Services/update-service.service";
import { Observable, of } from 'rxjs';
import * as _ from 'lodash';

@Component({
  selector: 'app-update-status',
  templateUrl: './update-status.component.html',
  styleUrls: []
})
/*
export class UpdateStatusModel {
  data: object;
  /!*id: number;
  client_id: number;
  hostname: string;
  update_version: string;
  status_reason: string;
  timestamp: string;*!/
}*/

export class UpdateStatusComponent implements OnInit {

  heading = 'Dashboard';
  subheading = 'This is an example dashboard created using build-in elements and components.';
  icon = 'pe-7s-plane icon-gradient bg-tempting-azure';

  public updatesStatuses$: Observable<any>;
  public _: any = _;

  constructor(private us: UpdateService) { }

  ngOnInit() {
      this.updatesStatuses$ = this.us.getUpdatesStatus();
  }

  getPage(page?: any) {
      this.updatesStatuses$ = this.us.getUpdatesStatus(page);
  }

  getGitUrl(version: string){
    if(version.indexOf('dirty') === -1){
        let versionArr = version.split('-');
        return `https://bitbucket.org/shield_meah/magen/commits/?search=${versionArr[versionArr.length-1]}`;
    }
    return null;
  }

}
