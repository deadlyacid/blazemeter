import { Component, OnInit } from '@angular/core';
import {UploadHistoryService} from "../../Services/upload-history-service.service";
import {TestsService} from "../../Services/tests.service";

@Component({
  selector: 'app-tests',
  templateUrl: './tests.component.html',
  styleUrls: ['./tests.component.sass']
})
export class TestsComponent implements OnInit {

  constructor(private testSrv: TestsService) { }

  ngOnInit() {
    this.testSrv.getTests().subscribe((res) => {
      console.log(res);
    });
  }

  logInWithFacebook() {
    // @ts-ignore
    window.FB.login((response) => {
      if (response.authResponse) {
        alert('You are logged in &amp; cookie set!');
        // Now you can redirect the user or do an AJAX request to
        // a PHP script that grabs the signed request from the co
       this.testSrv.getFBToken(response.authResponse).subscribe((result) => {
          console.log(result);
        });
      } else {
        alert('User cancelled login or did not fully authorize.');
      }
    });
    return false;
  }

}
