import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ClientsConfigurationService } from "../../Services/clients-configuration-service.service";
import { Observable, of } from 'rxjs';
import * as _ from 'lodash';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';


@Component({
    selector: 'app-update-status',
    templateUrl: './clients-config.component.html',
    styleUrls: ['./buttons.css']
})

export class ClientsConfigComponent implements OnInit {

    public clientsConfig$: any = null;
    public _: any = _;
    closeResult: string;
    configForm: FormGroup;
    submitted = false;

    constructor(private cc: ClientsConfigurationService,
                private modalService: NgbModal,
                private formBuilder: FormBuilder) { }

    ngOnInit() {
        this.clientsConfig$ = this.cc.getConfigs().subscribe((configData: any) => {
            this.clientsConfig$ = configData;
        });

        this.configForm = this.formBuilder.group({
            configName: ['', Validators.required],
            configAddress: ['', Validators.required]
        });
    }

    ngOnDestroy(): void {
        //this.clientsConfig$.next();
        //this.clientsConfig$.complete();
    }

    getPage(page?: any) {
        this.clientsConfig$ = this.cc.getConfigs(page).subscribe((configData: any) => {
            this.clientsConfig$ = configData;
        });
    }

    editConfig(event, item?){
        event.stopPropagation();
        if(item){
          item.isEditingMode = true;
        }
    }

    doNothing(){
        return;
    }

    deleteConfig(event, item){
        if(item && item.id){
            this.cc.deleteConfig(item.id).subscribe(res => {
                item.isEditingMode = false;
                this.clientsConfig$.data = this.clientsConfig$.data.filter(function( obj ) {
                    return obj.id !== item.id;
                });
            }, (err) => {
                console.log(err);
            });
        }
    }

    updateConfig(event, item, indx){
        event.stopPropagation();
        this.cc.updateConfig(item).subscribe(res => {
                item.isEditingMode = false;
            }, (err) => {
                console.log(err);
        });
    }

    openConfigModal(content) {
        this.modalService.open(content, {
            size: 'sm',
            windowClass: 'clients-config-modal'
        }).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });

    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    get f() { return this.configForm.controls; }

    onSubmitNewConfig() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.configForm.invalid) {
            return;
        }

        this.cc.saveConfig(this.configForm.value).subscribe(res => {
            this.modalService.dismissAll();
        }, (err) => {
            console.log(err);
        });
    }

}
