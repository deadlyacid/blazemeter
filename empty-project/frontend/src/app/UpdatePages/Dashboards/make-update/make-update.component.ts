import {Component, OnInit, Inject, ChangeDetectorRef} from '@angular/core';
import { DOCUMENT } from '@angular/common';

import {FormBuilder, FormGroup, Validators, ValidatorFn, FormArray, FormControl} from '@angular/forms';
import { LoadingBarService } from '@ngx-loading-bar/core';
import {MakeUpdateService} from "../../Services/make-update.service";
import {environment} from "../../../../environments/environment";
import {ClientsConfigurationService} from "../../Services/clients-configuration-service.service";
import { PageScrollService } from 'ngx-page-scroll-core';
import { saveAs } from 'file-saver';
import {SessionService} from "../../Services/session-service.service";
import Swal from 'sweetalert2'
import 'sweetalert2/src/sweetalert2.scss'

@Component({
  selector: 'app-make-update',
  templateUrl: './make-update.component.html',
  styleUrls: ['./make-update.css']
})
export class MakeUpdateComponent implements OnInit {

    public clientsConfig$: any = null;
    uri = environment.apiUri;
    createUpdateForm: FormGroup;
    gitDiffStepForm: FormGroup;
    updateSuccess = true;
    submitted = false;
    gitDiffStep = false;
    returnError = '';
    noGitChanges = false;

    gitDiffFiles = [];
    gitFormSubmitted = false;
    createUpdateFinished = false;
    selectedUploadServer: any;
    lastCommits = [];
    loadingLastCommits = true;
    isBusy = true;

      constructor(
          private cd: ChangeDetectorRef,
          private sessionSrv: SessionService,
          private formBuilder: FormBuilder,
          private fb: FormBuilder,
          private loadingBar: LoadingBarService,
          private cc: ClientsConfigurationService,
          private makeUpdateSrv: MakeUpdateService,
          private pageScrollService: PageScrollService, @Inject(DOCUMENT) private document: any
      ) { }

      ngOnInit() {
          this.isBusy = true;
          this.createUpdateForm = this.formBuilder.group({
              gitSourceCommit: ['', Validators.required],
              gitDestCommit: ['', Validators.required]
          });

          this.gitDiffStepForm = this.fb.group({
              gitDiffFiles: new FormArray(this.gitDiffFiles, this.minSelectedCheckboxes(1))
              //gitDiffFiles: this.buildItems()
              //gitDiffFiles: new FormArray([])
              //gitDiffFiles: ['', Validators.required]
          });

          this.clientsConfig$ = this.cc.getConfigs().subscribe((configData: any) => {
              this.clientsConfig$ = configData;
              this.selectedUploadServer = configData.data[0]['name'];
          });
          this.initForm();
      }

    ngAfterContentInit(){
        this.sessionSrv.isBusySub.subscribe(result => {
            this.isBusy = result;
            this.cd.detectChanges();
        });

    }

    initForm(){
        this.updateSuccess = true;
        this.submitted = false;
        this.gitDiffStep = false;
        this.returnError = '';
        this.gitDiffFiles = [];
        this.noGitChanges = false;
        this.gitFormSubmitted = false;
        this.createUpdateFinished = false;
        this.gitDiffStepForm.reset();
        this.createUpdateForm.reset();
        this.loadingBar.complete();
        this.loadingLastCommits = true;

        this.makeUpdateSrv.getLastCommits().subscribe((res: any) => {
            this.lastCommits = res.result;
            this.loadingLastCommits = false;
        });

    }

      get f() { return this.createUpdateForm.controls; }
      get fGit() { return this.gitDiffStepForm.get('gitDiffFiles'); }

      onSubmitNewUpdate() {
          this.loadingBar.start();
          this.submitted = true;

          // stop here if form is invalid
          if (this.createUpdateForm.invalid) {
              this.loadingBar.stop();
              return;
          }

          //this split is to set the hash value if select from last 10 commits
          this.createUpdateForm.controls.gitSourceCommit.setValue(this.createUpdateForm.value.gitSourceCommit.split(' ')[0]);
          this.createUpdateForm.controls.gitDestCommit.setValue(this.createUpdateForm.value.gitDestCommit.split(' ')[0]);
          this.makeUpdateSrv.prepareUpdate(this.createUpdateForm.value).subscribe((res: any) => {
              if(!res.success){
                  this.loadingBar.stop();
                  this.updateSuccess = false;
                  this.gitDiffStep = false;
                  this.gitDiffFiles = [];
                  this.returnError = res.result;
              }else {
                  if(res.result){
                      this.updateSuccess = true;
                      this.gitDiffStep = true;
                      this.gitDiffFiles = res.result;
                      this.noGitChanges = false;
                      this.gitDiffFiles.map((o, i) => {
                          if(o){
                              const control = new FormControl(o.checked); // if first item set to true, else false
                              (this.gitDiffStepForm.controls.gitDiffFiles as FormArray).push(control);
                          }
                      });
                  }else{
                        this.noGitChanges = true;
                  }
                  this.loadingBar.complete();
                  this.pageScrollService.scroll({
                      duration: 1500,
                      document: this.document,
                      scrollTarget: '#step-diff'
                    });
              }
          }, (err) => {
              console.log(err);
              this.loadingBar.stop();
          });
      }

    submitGitFilesDiff(){
        this.gitFormSubmitted = true;
        this.createUpdateFinished = false;
        this.loadingBar.start();
        // stop here if form is invalid
        if (this.gitDiffStepForm.invalid) {
            this.loadingBar.stop();
            return;
        }
       let uncheckedFiles = this.fGit.value.map((o,i) =>{
           if(!o){
               return this.gitDiffFiles[i].fileName;
           }
       });

        this.makeUpdateSrv.createUpdate(uncheckedFiles).subscribe((res: any) => {
            if(!res.success){
                this.loadingBar.stop();
                this.updateSuccess = false;
                this.returnError = res.result;
                this.gitFormSubmitted = false;
            }else {
                this.createUpdateFinished = true;
                this.gitFormSubmitted = true;
                this.updateSuccess = true;
                this.loadingBar.complete();
                this.pageScrollService.scroll({
                    duration: 1500,
                    document: this.document,
                    scrollTarget: '#step-download'
                });
            }
        }, (err) => {
            console.log(err);
            this.loadingBar.stop();
        });
    }

    submitUploadFile(){
          this.makeUpdateSrv.uploadToServer(this.selectedUploadServer).subscribe((res: any) =>{
              Swal.fire({
                  title: 'Upload Request Succeed!',
                  html: 'The table UpdatesStatuses will be update shortly.<br/>You\'ll be able to watch the final result in ' +
                  '<a class="vsm-link" routerlink="/pages/update-status" routerlinkactive="active-item" ng-reflect-router-link="/pages/update-status" href="/pages/update-status"><span class="vsm-title">Update Status</span></a> page.',
                  type: 'info',
                  confirmButtonText: '<i class="fa fa-thumbs-up"></i> Cool!'
              });
          });
    }

    downloadUpdate(){
        this.makeUpdateSrv.downloadUpdate().subscribe(data => {
            var blob = new Blob([data], {type: "application/gzip"});
            saveAs(blob, 'update-latest', {'Content-Type': "application/gzip"})
        },
        err=>{
                console.log(err);
            }
        );
    }

    minSelectedCheckboxes(min = 1) {
        const validator: ValidatorFn = (formArray: FormArray) => {
            //const totalSelected = formArray.controls.length;
            let isMinSelected = 0;
            formArray.controls.forEach((item) => {
                item.value ? isMinSelected++ : '';
            });
            return isMinSelected >= min ? null : { required: true };
        };

        return validator;
    }
}
