import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs/internal/BehaviorSubject";
import {interval, Observable, Subscription} from "rxjs/index";
import * as moment from 'moment';
import {environment} from "../../../environments/environment";
import {AuthenticationService} from "./authentication.service";

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  private _lastInteraction = new BehaviorSubject<string>('');
  private _maxInactiveTime = environment.maxInactiveMin;
  private _intervalCount;
  private authSrv: AuthenticationService;

  private isBusySubject: BehaviorSubject<boolean>;
  public isBusySub: Observable<any>;

  constructor(authSrv: AuthenticationService) {
    this.authSrv = authSrv;

    this.isBusySubject = new BehaviorSubject<boolean>(false);
    this.isBusySub = this.isBusySubject.asObservable();
  }

  setInteraction(newInteraction: string){
      this._lastInteraction.next(newInteraction);
  }

  startExpireCheck () {
      if(this.authSrv.isLoggedIn){
          this._intervalCount = interval(2000).subscribe(() => {
              this.sessionExpiredCheck();
          });
      }
  }

  setIsBusy(value){
      this.isBusySubject.next(value);
  }

  stopExpireCheck(){
    this._intervalCount.unsubscribe();
  }

  sessionExpiredCheck(){
      let maxWaitTime = moment().subtract(this._maxInactiveTime, 'minutes');
      let currLastInteraction = this._lastInteraction.getValue();
      if( moment(currLastInteraction) < maxWaitTime) {
          this.stopExpireCheck();
          this.authSrv.logout('Session Expired');
      }else{
          this.authSrv.getConnectedUsers().subscribe((users) =>{
              this.authSrv.updateConnectedUsers(users);
          });
      }
  }

}
