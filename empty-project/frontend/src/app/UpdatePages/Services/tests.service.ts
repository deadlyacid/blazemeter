import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TestsService {
  //uri = environment.apiUri;
  uri = environment.nodeUri;
  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };
  httpDownloadOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/gzip'})
  };

  constructor(private http: HttpClient) {}

  getTests() {
    return this.http.get(`${this.uri}/tests/`, this.httpOptions);
  }

  getFBToken(authResponse: string) {
    const authObj = JSON.stringify(authResponse);
    const params = new HttpParams();
    params.set(authResponse, authObj);
    return this.http.post(`${this.uri}/fb/`, authResponse, this.httpOptions);
  }


}
