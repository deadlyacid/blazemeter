import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MakeUpdateService {
    uri = environment.apiUri;
    httpOptions = {
        headers: new HttpHeaders({'Content-Type': 'application/json'})
    };
    httpDownloadOptions = {
        headers: new HttpHeaders({'Content-Type': 'application/gzip'})
    };

    constructor(private http: HttpClient) {}

    prepareUpdate(item: any) {
        return this.http.post(`${this.uri}/prepare-update/`, item, this.httpOptions)
    }

    createUpdate(items: object) {
        return this.http.post(`${this.uri}/create-update/`,  {uncheckedFiles: items}, this.httpOptions)
    }

    uploadToServer(serverName: string) {
        return this.http.post(`${this.uri}/upload-update/`,  {serverName: serverName}, this.httpOptions)
    }

    downloadUpdate(){
        return this.http.get(`${this.uri}/download-update/`, {responseType: 'blob', headers:  this.httpDownloadOptions.headers});
    }

    getLastCommits(){
        return this.http.get(`${this.uri}/commits`, this.httpOptions);
    }

}