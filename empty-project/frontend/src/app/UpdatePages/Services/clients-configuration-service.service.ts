import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from '../../../environments/environment';
import {AuthenticationService} from "./authentication.service";

@Injectable({
    providedIn: 'root'
})

export class ClientsConfigurationService {

    uri = environment.apiUri;
    httpOptions = {
        headers: new HttpHeaders({'Content-Type': 'application/json'})
    };
    token: string;

    constructor(private http: HttpClient, private authSrv: AuthenticationService) {}

    getConfigs(page?:any): Observable<any>{
        this.token = this.authSrv.currentToken;
        if(page && isNaN(page)){
            return this.http.get(`${page}&token=${this.token}`);
        }
        if(page){
            return this.http.get(`${this.uri}/config?page=${page}&token=${this.token}`);
        }
        return this.http.get(`${this.uri}/config`);
    }

    updateConfig(item: any){
        return this.http.put(`${this.uri}/config/${item.id}`, item, this.httpOptions)
    }

    deleteConfig(itemId: number){
        return this.http.delete(`${this.uri}/config/${itemId}`, this.httpOptions)
    }

    saveConfig(item: any){
        return this.http.post(`${this.uri}/config/`, item, this.httpOptions)
    }

}
