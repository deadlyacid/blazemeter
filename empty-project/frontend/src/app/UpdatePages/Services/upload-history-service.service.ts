import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { Observable, of } from 'rxjs';
import { environment } from '../../../environments/environment';
import {AuthenticationService} from "./authentication.service";

@Injectable({
  providedIn: 'root'
})
export class UploadHistoryService {

  uri = environment.apiUri;
  token: string = '';
  constructor(private http: HttpClient, private authSrv: AuthenticationService) {}

  getUploadHistories(page?:any): Observable<any>{
      this.token = this.authSrv.currentToken;
      if(page && isNaN(page)){
          return this.http.get(`${page}&token=${this.token}`);
      }
      if(page){
          return this.http.get(`${this.uri}/history?page=${page}&token=${this.token}`);
      }
      return this.http.get(`${this.uri}/history`);
  }
}
