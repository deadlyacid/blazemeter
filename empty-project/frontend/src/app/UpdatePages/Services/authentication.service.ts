import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { User } from '../Models/user'
import {environment} from "../../../environments/environment";

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;
    public currentToken: string;
    public refreshToken: string;

    uri = environment.apiUri;
    private connectedUsersSubject: BehaviorSubject<any>;
    public connectedUsers: Observable<any>;

    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();

        this.connectedUsersSubject = new BehaviorSubject<any>([]);
        this.connectedUsers = this.connectedUsersSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    public get isLoggedIn(): boolean {
        return this.currentUserSubject.value !== null;
    }

    public get authToken(): string{
        return this.currentToken;
    }

    /*public get refreshToken(): string{
        return this.refreshToken;
    }*/

    updateConnectedUsers(newVal){
        this.connectedUsersSubject.next(newVal);
    }

    setLoggedIn(user: any) {
        if (user && user.token) {
            localStorage.removeItem('reason');
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem('currentUser', JSON.stringify(user));
            this.currentToken = user.token;
            this.refreshToken = user.refreshToken;
            this.currentUserSubject.next(user);
            return user;
        }
        return null;
    }

    logout(reason?: string) {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        if(reason){
            localStorage.setItem('reason', reason);
        }
        location.href = environment.apiLogout;
        this.currentUserSubject.next(null);
    }

    getConnectedUsers(){
        return this.http.get(`${this.uri}/getConnectedUsers`);
    }
}