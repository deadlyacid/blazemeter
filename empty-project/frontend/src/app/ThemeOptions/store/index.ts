import { combineReducers } from 'redux';
import { ConfigReducer } from './config.reducer';

export class MagenUpdateState {
  config;
};

export const rootReducer = combineReducers<MagenUpdateState>({
  config: ConfigReducer,
});


