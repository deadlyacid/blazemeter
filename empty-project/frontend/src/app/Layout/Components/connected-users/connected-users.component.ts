import {Component, Input, OnInit} from '@angular/core';
import { SessionService } from '../../../UpdatePages/Services/session-service.service';
import {AuthenticationService} from "../../../UpdatePages/Services/authentication.service";
import { Subscription} from "rxjs/index";
import * as _ from 'lodash';
import {isBoolean} from "util";

@Component({
  selector: 'app-connected-users',
  templateUrl: './connected-users.component.html',
  styleUrls: []
})
export class ConnectedUsersComponent implements OnInit {
  private sessionSrv: SessionService;
  connectedUsers$: Subscription;
  public users = [];

  constructor(sessionSrv: SessionService, private authSrv: AuthenticationService) {
      this.sessionSrv = sessionSrv;
  }

  @Input() set lastInteraction(value: string){
      this.sessionSrv.setInteraction(value);
  }

  ngOnInit() {
      this.sessionSrv.startExpireCheck();
      this.connectedUsers$ = this.authSrv.connectedUsers.subscribe(result => {
          this.users = _.merge(this.users, result);
      });
  }

  busyMark(user){
      if(isBoolean(user.is_busy)){
          if (this.authSrv.currentUserValue.name !== user.name){
              this.sessionSrv.setIsBusy(user.is_busy);
          }
          return user.is_busy;
      }
      return false;
  }

  ngOnDestroy(){
      this.connectedUsers$.unsubscribe();
  }

}
