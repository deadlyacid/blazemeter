import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from "../../../../../UpdatePages/Services/authentication.service";
import {log} from "util";

@Component({
  selector: 'app-search-box',
  templateUrl: './search-box.component.html',
})
export class SearchBoxComponent implements OnInit {

  public isActive: any;
  public name: string = null;

  constructor(private authSrv: AuthenticationService) {
  }

  ngOnInit() {
    const loggedUser = this.authSrv.currentUserValue;
    if(loggedUser && loggedUser.name){
      this.name = loggedUser.name;
    }

  }

}
