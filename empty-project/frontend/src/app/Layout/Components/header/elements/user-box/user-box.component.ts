import {Component, OnInit} from '@angular/core';
import {ThemeOptions} from '../../../../../theme-options';
import {AuthenticationService} from "../../../../../UpdatePages/Services/authentication.service";

@Component({
  selector: 'app-user-box',
  templateUrl: './user-box.component.html',
})
export class UserBoxComponent implements OnInit {

  public userPhoto: string = './assets/images/avatars/1.jpg';
  public isLoggedOut: boolean;
  constructor(public globals: ThemeOptions,
              private authSrv: AuthenticationService) {
  }

  ngOnInit() {
    const user = this.authSrv.currentUserValue;
    if(user && user.avatar){
        this.userPhoto = user.avatar;
    }
    this.isLoggedOut = this.authSrv.currentUserValue === null;
  }

  makeLogin(){
    location.href = '/pages/login-boxed';
  }

  makeLogout(){
    this.authSrv.logout();
  }

}
