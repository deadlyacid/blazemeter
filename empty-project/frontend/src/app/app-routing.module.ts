import { NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {BaseLayoutComponent} from './Layout/base-layout/base-layout.component';
import {PagesLayoutComponent} from './Layout/pages-layout/pages-layout.component';

import {LoginBoxedComponent} from './UpdatePages/UserPages/login-boxed/login-boxed.component';

import {UpdateStatusComponent} from "./UpdatePages/Dashboards/update-status/update-status.component";
import {UploadHistoryComponent} from "./UpdatePages/Dashboards/upload-history/upload-history.component";
import {ClientsConfigComponent} from "./UpdatePages/Dashboards/clients-config/clients-config.component";
import {MakeUpdateComponent} from "./UpdatePages/Dashboards/make-update/make-update.component";
import { AuthGuard } from './_guards/auth.guard';
import {LoggedInComponent} from "./UpdatePages/UserPages/logged-in/logged-in.component";
import {TestsComponent} from "./UpdatePages/Dashboards/tests/tests.component";

const routes: Routes = [
  {
    path: '',
    component: BaseLayoutComponent,
    children: [


      // Dashboads
      {path: '', component: UpdateStatusComponent, data: {extraParameter: 'dashboardsMenu'}},
      // Update System
      {path: 'pages/update-status', component: UpdateStatusComponent, data: {extraParameter: 'dashboardsMenu'}},
      {path: 'pages/upload-history', component: UploadHistoryComponent, data: {extraParameter: 'dashboardsMenu'}},
      {path: 'pages/clients-config', component: ClientsConfigComponent, data: {extraParameter: 'pagesMenu'}},
      {path: 'pages/make-update', component: MakeUpdateComponent, data: {extraParameter: 'dashboardsMenu'}},
    ],
    //canActivate: [AuthGuard]
  },
  {
    path: '',
    component: PagesLayoutComponent,
    children: [
    // User Pages
    {path: 'tests', component: TestsComponent },
    {path: 'pages/login-boxed', component: LoginBoxedComponent },
    {path: 'pages/logged-in/:user', component: LoggedInComponent, data: {extraParameter: ''}},
    ]
  },
  {path: '**', redirectTo: ''}
];

//const isDev = isDevMode();
@NgModule({
  imports: [RouterModule.forRoot(routes,
    {
      scrollPositionRestoration: 'enabled',
      anchorScrolling: 'enabled',
      enableTracing: false

    })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
