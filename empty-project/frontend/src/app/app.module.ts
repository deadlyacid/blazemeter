import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgReduxModule} from '@angular-redux/store';
import {NgRedux, DevToolsExtension} from '@angular-redux/store';
import {MagenUpdateState, rootReducer} from './ThemeOptions/store';
import {ConfigActions} from './ThemeOptions/store/config.actions';
import {AppRoutingModule} from './app-routing.module';
import {LoadingBarRouterModule} from '@ngx-loading-bar/router';

import {CommonModule} from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {AppComponent} from './app.component';
import { ErrorInterceptor } from './interceptors/error.interceptor';
import { NgxPageScrollCoreModule } from 'ngx-page-scroll-core';
import { NgSelectModule } from '@ng-select/ng-select';

// BOOTSTRAP COMPONENTS

import {AngularFontAwesomeModule} from 'angular-font-awesome';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {PERFECT_SCROLLBAR_CONFIG} from 'ngx-perfect-scrollbar';
import {PerfectScrollbarConfigInterface} from 'ngx-perfect-scrollbar';

// LAYOUT

import {BaseLayoutComponent} from './Layout/base-layout/base-layout.component';
import {PagesLayoutComponent} from './Layout/pages-layout/pages-layout.component';
import {PageTitleComponent} from './Layout/Components/page-title/page-title.component';

// HEADER
import {HeaderComponent} from './Layout/Components/header/header.component';
import {SearchBoxComponent} from './Layout/Components/header/elements/search-box/search-box.component';
import {UserBoxComponent} from './Layout/Components/header/elements/user-box/user-box.component';

// SIDEBAR
import {SidebarComponent} from './Layout/Components/sidebar/sidebar.component';
import {LogoComponent} from './Layout/Components/sidebar/elements/logo/logo.component';

// FOOTER
import {FooterComponent} from './Layout/Components/footer/footer.component';

// Pages
import {LoginBoxedComponent} from './UpdatePages/UserPages/login-boxed/login-boxed.component';
import { UpdateService } from './UpdatePages/Services/update-service.service';
import { UploadHistoryService } from './UpdatePages/Services/upload-history-service.service';
import { ClientsConfigurationService } from './UpdatePages/Services/clients-configuration-service.service';


import { UploadHistoryComponent } from './UpdatePages/Dashboards/upload-history/upload-history.component';
import { ClientsConfigComponent } from './UpdatePages/Dashboards/clients-config/clients-config.component';
import { UpdateStatusComponent } from './UpdatePages/Dashboards/update-status/update-status.component';
import { MakeUpdateComponent } from './UpdatePages/Dashboards/make-update/make-update.component';

import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { LoggedInComponent } from './UpdatePages/UserPages/logged-in/logged-in.component';
import { ConnectedUsersComponent } from './Layout/Components/connected-users/connected-users.component';
import { TestsComponent } from './UpdatePages/Dashboards/tests/tests.component';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

@NgModule({
  declarations: [
    // LAYOUT
    AppComponent,
    BaseLayoutComponent,
    PagesLayoutComponent,
    PageTitleComponent,

    // HEADER
    HeaderComponent,
    SearchBoxComponent,
    UserBoxComponent,

    // SIDEBAR
    SidebarComponent,
    LogoComponent,

    // FOOTER
    FooterComponent,

    // User Pages
    LoginBoxedComponent,
    UpdateStatusComponent,
    UploadHistoryComponent,
    ClientsConfigComponent,
    MakeUpdateComponent,
    LoggedInComponent,
    ConnectedUsersComponent,
    TestsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgReduxModule,
    CommonModule,
    LoadingBarRouterModule,

    // Angular Bootstrap Components
    PerfectScrollbarModule,
    NgbModule,
    AngularFontAwesomeModule,
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    SweetAlert2Module.forRoot(),
    NgxPageScrollCoreModule
  ],
  providers: [
    {
      provide:
      PERFECT_SCROLLBAR_CONFIG,
      // DROPZONE_CONFIG,
      useValue:
      DEFAULT_PERFECT_SCROLLBAR_CONFIG,
      // DEFAULT_DROPZONE_CONFIG,
    },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    ConfigActions,
    UpdateService,
    UploadHistoryService,
    ClientsConfigurationService

  ],
  bootstrap: [AppComponent]
})



export class AppModule {
  constructor(private ngRedux: NgRedux<MagenUpdateState>,
              private devTool: DevToolsExtension) {

    this.ngRedux.configureStore(
      rootReducer,
      {} as MagenUpdateState,
      [],
      [devTool.isEnabled() ? devTool.enhancer() : f => f]
    );

  }
}
