import { Injectable } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpSentEvent,
    HttpHeaderResponse, HttpProgressEvent, HttpResponse, HttpUserEvent, HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { AuthenticationService } from '../UpdatePages/Services/authentication.service';
import {environment} from "../../environments/environment";

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    isRefreshingToken: boolean = false;


    constructor(private authenticationService: AuthenticationService) { }

    addToken(req: HttpRequest<any>, token: string): HttpRequest<any> {
        const user: any = this.authenticationService.currentUserValue;
        if(user && user.token){
            return req.clone({ setHeaders: { Authorization: 'Bearer ' + user.token }})
        }
        return req.clone();
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        console.log('skipping token check..');
        return next.handle(request);
        /*return next.handle(this.addToken(request, this.authenticationService.authToken))
            .pipe(catchError(err => {
                console.log(err);
                if(err.status === 401){
                    this.authenticationService.logout();
                }

                const error = err.error.message || err.statusText;
                return throwError(error);
            }));*/
    }
}
