<?php


use Illuminate\Http\Request;

Route::get('tests', function (){
    $all = \App\Models\Vote::all();

    return response()->json([$all, 'Yeahhhh'.time()]);
});

Route::get('info', function (){
    phpinfo();
});

Route::prefix('api')->middleware(['cors'])->group(function () {
    Route::get('tests', function (){
        return response()->json('Yeahhhh'.time());
    });

    Route::post('fb', function (Request $request){
        return 1;
        $response = [];
        $all = $request->all();
        if(count($all) && $all['accessToken']){
            $jsAccessToken = $all['accessToken'];
        }

        $secret = '6f80363b4adf98ffb7e0bf81ac463836';
        $fb = new Facebook\Facebook([
            'app_id' => '2435090263378080',
            'app_secret' => $secret,
            'default_graph_version' => 'v5.0',
        ]);

        $helper = $fb->getJavaScriptHelper();

        try {
            $accessToken = $helper->getAccessToken();
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        if (! isset($accessToken) && isset($jsAccessToken)) {
            $fb->setDefaultAccessToken($jsAccessToken);
            try{
                $response = $fb->get('/me?fields=id,name,email', $jsAccessToken);
            }catch (\Facebook\Exceptions\FacebookResponseException $e){
                var_dump($e);
            }catch (\Facebook\Exceptions\FacebookSDKException $ex){
                var_dump($ex);
            }

            return $response->getBody();

        }

    });
});
