<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UploadHistory extends Model
{
    protected $table = 'UpdatesUploadHistory';
    protected $hidden = ['client_id'];

    public function ClientsConfiguration(){
        return $this->hasOne(ClientsConfiguration::class, 'id', 'client_id');
    }
}
