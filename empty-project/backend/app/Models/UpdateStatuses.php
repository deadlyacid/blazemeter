<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UpdateStatuses extends Model
{
    protected $table = 'UpdatesStatuses';
    protected $hidden = ['client_id'];
    public function ClientsConfiguration(){
        return $this->hasOne(ClientsConfiguration::class, 'id', 'client_id');
    }
}
