<?php

namespace App\Http\Controllers;

use App\Models\UpdateStatuses;

class UpdatesController extends Controller
{
    public function index(){
        return UpdateStatuses::with('ClientsConfiguration')->orderBy('timestamp', 'DESC')->paginate(15);
    }
}
