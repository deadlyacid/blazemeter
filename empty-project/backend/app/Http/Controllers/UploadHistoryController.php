<?php

namespace App\Http\Controllers;

use App\Models\UpdateStatuses;
use App\Models\UploadHistory;

class UploadHistoryController extends Controller
{
    public function index(){
        return UploadHistory::with('ClientsConfiguration')->orderBy('timestamp', 'DESC')->paginate(15);
    }
}
