<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/pages/login-boxed';
    protected $loginPath = '/pages/login-boxed';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
        $url = env('GOOGLE_REDIRECT');
        return Socialite::driver('google')
            ->with(
                ['client_id' => '307436073549-hoagdh0m25ran0jrttv3v2cja4l80024.apps.googleusercontent.com'],
                ['client_secret' => 'HaVmFXzEHH2LW_MWqqO8DcBC'],
                ['redirect' => $url])
            ->stateless()->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {

        $googleUser = Socialite::driver('google')->stateless()->user();
        if($googleUser && $googleUser->token){
            $existUser = User::where('email',$googleUser->email)->first();

            if($existUser) {
                Auth::loginUsingId($existUser->id);
                $existUser->touch();
                $connectedUsers = User::select('name', 'avatar')->where('updated_at', '>=', Carbon::now()->subMinute(3)->toDateTimeString())->get()->toArray();
                $existUser->connectedUsers = $connectedUsers;

                unset($existUser->password);
                unset($existUser->created_at);
                unset($existUser->deleted_at);
                unset($existUser->id);
                $url = env('REDIRECT_AFTER_LOGGEDIN').base64_encode(json_encode($existUser));
                return redirect($url);
            }

            $user = new User;
            $user->name = $googleUser->name;
            $user->email = $googleUser->email;
            $user->avatar = $googleUser->avatar;
            $user->google_id = $googleUser->id;
            $user->password = md5(rand(1,10000));
            $user->token = $googleUser->token;
            $user->save();

            Auth::login($user);
            $connectedUsers = User::select(['name', 'avatar'])->where('updated_at', '>=', Carbon::now()->subMinute(3)->toDateTimeString())->get();
            $user->connectedUsers = $connectedUsers;
            unset($user->password);
            unset($user->created_at);
            unset($user->deleted_at);
            unset($user->id);
            $url = env('REDIRECT_AFTER_LOGGEDIN').base64_encode(json_encode($user));
            return redirect($url);

        }
    }

    public function login(){
        $url = env('REDIRECT_AFTER_LOGGOUT');
        return redirect($url);
    }

    public function logout(){
        $connectedUser = Auth::user();
        if($connectedUser){
            User::where('busy', '<>', null)
                ->where('name', $connectedUser->name)
                ->update(['busy' => null]);
        }
        Auth::logout();
        $url = env('REDIRECT_AFTER_LOGGOUT');
        return redirect($url);
    }
}
