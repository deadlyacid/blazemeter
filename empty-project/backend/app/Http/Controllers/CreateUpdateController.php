<?php

namespace App\Http\Controllers;

use App\Models\ClientsConfiguration;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\Process\Process;

class CreateUpdateController extends Controller
{

    const SCRIPT_BEGIN = 'python2.8 ';
    const PREPARE_UPDATE = 'prepare_update.py';
    const CREATE_TAR_UPDATE = 'create_system_update.py';
    const UPLOAD_UPDATE = 'upload_update.py';
    const UPDATE_STATUS = 'download_updates_statuses.py';
    const UNRELEVANT_FILES = ['postinst', 'postrm', 'preinst', 'prerm'];
    const PATH_TRANSLATOR = [
                                'pymitm' => 'opt/magen/bin/pymitm',
                                'web_api_v2' => 'var/www/html/magen',
                                'achilles' => 'opt/magen/achilles',
                            ];
    private $prepareUpdate;
    private $createUpdate;
    private $scriptsPath;
    private $uploadUpdate;
    private $updateStatus;

    public function __construct() {
        //User::where('id', $connectedUser)->update('is_busy', true);
        $this->scriptsPath = env('UPDATE_SCRIPTS', null);
        if($this->scriptsPath){
            $this->prepareUpdate = $this->scriptsPath.self::PREPARE_UPDATE;
            $this->createUpdate = $this->scriptsPath.self::CREATE_TAR_UPDATE;
            $this->uploadUpdate = $this->scriptsPath.self::UPLOAD_UPDATE;
            $this->updateStatus = $this->scriptsPath.self::UPDATE_STATUS;

            if(!file_exists($this->prepareUpdate)){
                throw new \Exception("Script doesn't exists! $this->prepareUpdate");
            }
            $this->prepareUpdate = self::SCRIPT_BEGIN . $this->prepareUpdate;
            $this->createUpdate = self::SCRIPT_BEGIN . $this->createUpdate;
            $this->uploadUpdate = self::SCRIPT_BEGIN . $this->uploadUpdate;
            $this->updateStatus = self::SCRIPT_BEGIN . $this->updateStatus;
        }
    }

    public function getCommits(){
        $gitCommitsCmd = 'cd /home/me/workspace/magen && git log --abbrev-commit --pretty=oneline -n 10  --no-merges';
        $process = new Process($gitCommitsCmd);
        $process->run();
        $resultCode = $process->getExitCode();

        if($resultCode !== 0){
            return [
                'success'   => false,
                'cmd'   => $gitCommitsCmd,
                'result'    => json_encode($process->getErrorOutput())
            ];
        }
        $result = explode("\n", $process->getOutput());

        return [
            'success'   => true,
            'cmd'       => $gitCommitsCmd,
            'result'    =>  $result
        ];
    }

    public function store(Request $request){
        $inputs = $request->only(['gitSourceCommit', 'gitDestCommit']);
        $connectedUser = Auth::user();
        if($connectedUser){
            $checkWhoBusy = User::select('busy')->get()->first();
            if ($checkWhoBusy->is_busy && ($checkWhoBusy->is_busy !== $connectedUser->name)){
                return [
                    'success'   => false,
                    'cmd'   => '',
                    'result'    => "User: $checkWhoBusy->is_busy is currently using the system, ask them to logout."
                ];
            }
        }

        if(isset($inputs['gitSourceCommit']) && isset($inputs['gitDestCommit'])){
            Auth::User()->setUserBusy();
            $source = trim($inputs['gitSourceCommit']);
            $dest = trim($inputs['gitDestCommit']);
            $prepareUpdateCmd = "cd /home/me/workspace/magen && $this->prepareUpdate -s $source -d $dest";

            $process = new Process($prepareUpdateCmd);
            $process->run();
            $resultCode = $process->getExitCode();

            if($resultCode !== 0){
                return [
                    'success'   => false,
                    'cmd'   => $prepareUpdateCmd,
                    'result'    => json_encode($process->getErrorOutput())
                ];
            }
            else{

                $giDiffCmd = "cd /home/me/workspace/magen && git diff --name-only  $source $dest";
                $process2 = new Process($giDiffCmd);
                $process2->run();
                $diffFiles = $process2->getOutput();
                $diffFiles = explode("\n", $diffFiles);

                $diffFilesArr = array_map(function($item){
                    if(!in_array($item, self::UNRELEVANT_FILES) && strlen(trim($item))>0){
                        return ['fileName' => $item, 'checked' => true];
                    }
                }, $diffFiles);
                return [
                    'success'   => true,
                    'result'    => $diffFilesArr[0] !== null ? $diffFilesArr : null
                ];
            }
        }

        return null;
    }

    public function create(Request $request){
        $deleteFailed = [];
        $filesToRemove = $request->input('uncheckedFiles');

        $createUpdateCmd = "cd $this->scriptsPath && $this->createUpdate";
        $processCreateUpdate = new Process($createUpdateCmd);
        $processCreateUpdate->run();
        $processCreateUpdateResult = $processCreateUpdate->getOutput();

        $failReason = $this->checkResult($processCreateUpdateResult);
        if(!$failReason && count($filesToRemove)>0){
            $filePermissions = file_get_contents($this->scriptsPath . 'update_in/file_permissions');
            $filePermissionsArr = json_decode($filePermissions, true);

            foreach ($filesToRemove as $key => $currFile){
                if(!$currFile) continue;

                $headDir = explode('/', $currFile)[0];
                if(isset(self::PATH_TRANSLATOR[$headDir])) {
                    //set the correct path inside the filesystem folder
                    $currFile = str_replace($headDir, self::PATH_TRANSLATOR[$headDir], $currFile);
                }

                //replace the py with mgm extension
                $fileToRemove = str_replace('.py', '.mgm', $currFile);
                //point to the right path inside magen_update folder
                $fileToRemove = $this->scriptsPath . 'update_in/filesystem/' . $fileToRemove;

                if(file_exists($fileToRemove)){
                    $success = unlink($fileToRemove);
                    if(!$success) $deleteFailed[] = $fileToRemove;
                    else{
                        if(isset($filePermissionsArr[$currFile])){
                            unset($filePermissionsArr[$currFile]);
                        }
                    }
                }
            }

            $filePermissions = json_encode($filePermissionsArr, JSON_UNESCAPED_UNICODE);
            $fp = fopen($this->scriptsPath . 'update_in/file_permissions', 'w');
            fwrite($fp, $filePermissions);
            fclose($fp);
        }

        if($failReason !== null || count($deleteFailed)>0){
            return [
                'success'       => false,
                'result'        => $failReason,
                'deleteFailed'  => $deleteFailed
            ];
        }

        return [
            'success'       => true,
            'deleteFailed'  => $deleteFailed
        ];
    }

    public function downloadUpdateFile(){
        $updatedFile = '/home/me/workspace/magen/magen_update/update_out/update-latest';
        if (!is_dir($updatedFile) && file_exists($updatedFile)) {
            return response()->file($updatedFile, ['Content-Type: application/gzip']);
        }
        return null;
    }
    private function checkResult($result){
        $failReason = null;
        if (strpos($result, 'Failed obfuscating') !== false){
            $failReason = 'Failed obfuscating files, maybe php files are already obfuscated.';
        }

        return $failReason;
    }

    public function uploadUpdate(Request $request){
        $selectedServer = $request->only('serverName');
        $selectedServer = $selectedServer['serverName'];
        $createUpdateCmd = "cd $this->scriptsPath && $this->uploadUpdate -c $selectedServer";
        $processUpdate = new Process($createUpdateCmd);
        $processUpdate->run();
        $uploadResult = $processUpdate->getOutput();

        $updateStatusCmd = "cd $this->scriptsPath && $this->updateStatus -c $selectedServer";
        $updateStatus = new Process($updateStatusCmd);
        $updateStatus->run();
        return [
            'success'   => true,
            'cmd'       =>  $createUpdateCmd,
            'result'    => $uploadResult
        ];
    }

}
