<?php

namespace App\Http\Controllers;

use App\Models\ClientsConfiguration;
use Illuminate\Http\Request;

class ClientsConfigController extends Controller
{
    public function index(){
        return ClientsConfiguration::orderBy('name')->paginate(15);
    }

    public function update($id, Request $request){
        $params = $request->only(['id', 'name', 'address']);
        if($id == $params['id']){
            return ClientsConfiguration::where('id', $id)
                ->update(['name' => $params['name'], 'address' => $params['address']]);
        }
        return ['status' => false];
    }

    public function store(Request $request){
        $inputs = $request->only(['configName', 'configAddress']);

        $cc = new ClientsConfiguration();
        $cc->name = $inputs['configName'];
        $cc->address = $inputs['configAddress'];
        $cc->save();
    }

    public function destroy($id){
        return ClientsConfiguration::destroy($id);
    }
}
