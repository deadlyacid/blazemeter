<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class UpdateLoginDate
{

    public function handle($request, Closure $next)
    {
        if(Auth::user()){
            Auth::user()->touch();
        }
        return $next($request);
    }
}
