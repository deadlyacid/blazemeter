<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    public $timestamps = true;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','updated_at', 'is_busy'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'api_token' => 'token'
    ];

    protected $appends = [
        'is_busy'
    ];

    public function getIsBusyAttribute(){
        $usingSysTimeout = env('USING_SYS_TIMEOUT', 20);
        $isBusy = User::where('busy', '<>', null)->get()->first();
        if(empty($isBusy)){
            return false;
        }else{
            if($isBusy->id !== $this->id){
                $howLongBusy = $isBusy->updated_at->diffInMinutes(Carbon::now());
                if($howLongBusy >= $usingSysTimeout){
                    User::where('busy', '<>', null)->update(['busy' => null]);
                    return false;
                }
                return $this->attributes['is_busy'] = $isBusy->name;
            }else{
                return true;
            }
        }
    }

    public function scopeSetUserBusy(){
        User::where('busy', '<>', null)->update(['busy' => null]);
        $this->busy = true;
        $this->save();
    }
}
