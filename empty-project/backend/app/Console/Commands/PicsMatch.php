<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class PicsMatch extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'PicsMatch:start {path?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    private $picsPath;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $picsPath = $this->argument('path');
        if(!isset($picsPath)){
            $this->picsPath = __DIR__.'/../../../tests/photos';
        }

        if(is_dir($this->picsPath)){
            $filesA = scandir($this->picsPath.'/A');
            $filesB = scandir($this->picsPath.'/B');

            $filesA = array_flip($filesA);
            $filesB = array_flip($filesB);


            foreach ($filesA as $itemA => $test){
                if($itemA === '.' || $itemA === '..' || $itemA === '%') continue;

                if (isset($filesB[$itemA])) {
                    //$this->info('Found matching images: '. $itemA . ' about to merge.');
                    $this->mergeImages($itemA);
                }
            }
        }
    }

    private function mergeImages($imageA){
        $outName = $imageA;
        $imageA = $this->picsPath.'/A/'.$imageA;
        $imageB = $this->picsPath.'/B/'.$outName;

        $srcA = imagecreatefromjpeg($imageA);
        $srcB = imagecreatefromjpeg($imageB);

        $width_src = imagesx($srcA);
        $height_src = imagesy($srcA);
        $img = imagecreatetruecolor($width_src*2, $height_src);

        imagecopy($img, $srcA, 0, 0, 0 , 0, $width_src, $height_src);

        imagecopy($img, $srcB, $width_src, 0, 0 , 0, $width_src, $height_src);

        $outFile = $this->picsPath.'/AB/'.$outName;
        imagejpeg($img, $outFile, 100);

        $size = filesize($outFile);
        //$this->info('Image size is: ' . $size . ', About to reduce quality.');

        $this->reduceQuality($img, $size, $outName);

        //$this->line('-------');

    }

    private function reduceQuality($img, $originalSize, $outName){
        $goodQuality = true;
        $firstTime = true;
        $quality = 100;
        $prevSize = 0;

        while ($goodQuality){
            $quality-=3;

            imagejpeg($img, $this->picsPath . '/AB/tmp/' . $outName, $quality);
            $afterSize = filesize($this->picsPath.'/AB/tmp/'.$outName);
            if($firstTime){
                $firstTime = false;
                $prevSize = $originalSize;
            }

            $diff = (abs($prevSize - $afterSize));
            $percentage = ($diff/$prevSize)*100;

            if($percentage <= 5){
                $diff = (abs($originalSize - $afterSize));
                $percentageFromOrg = ($diff/$originalSize)*100;
                $this->info("$outName: saved $afterSize bytes, before: $originalSize bytes ($percentageFromOrg)");
                //$this->info('Reduce is complete - was: '.$beforeSize.', Now it\'s: '.$afterSize);
                rename($this->picsPath.'/AB/tmp/'.$outName, $this->picsPath.'/AB/'.$outName);
                $goodQuality = false;
            }

            try{
                unlink($this->picsPath.'/AB/tmp/'.$outName);
            }catch (\Exception $ex){}
            $prevSize = $afterSize;

        }

    }

}

