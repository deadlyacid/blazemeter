
Project Structure:
------------------

Since I had to upload all via ftp server I had no control over the web-server configuration.

- index.php is acting as front controller design pattern.

All the requests are going through this design-pattern and being handled with the Routes operation.

- become                (Main folder)

    -app                (PHP Server side classes)
    
        -Config         (All keys, passwords or any other configs)
        -Controllers    (All controllers MVC pattern)
        -Operations     (Requests & Routes classes)
        -Services       (DBs, Users, Curls requests classes)
        
    -public             (All public assets images/php files)
    
        -assets         (Public assets - js, css, images)
        
    -vendor             (3rd parties or any npm packages)
    
    -index.php          (Front controller)

In `app/Services/DB.php` you may find the DB handler which handle the DB connection
and sql statements.
Here I used `PDO` instance as singletone design pattern.


Each route navigate to its right destination
 - An API request goes to /api/* and being handle by the controller.
 - A View request goes to /public/* and being generated by its file.
 
 The JS application file is in assets where it holds a "Become" js class
 with few methods.
 
 Those methods are being called from the VIEW php file.
 
 For Example:
 public/stats.php is a view file for the stat page.
 
inside the `<script>` block there's a call for the right method:

```$xslt
$( document ).ready(function() {
        let becomeApp = new BecomeApp();
        becomeApp.getStatistics();
    });
```

