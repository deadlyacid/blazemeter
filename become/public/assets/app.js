let loading = true;
function drawCities(usersByCity) {
    Highcharts.chart('bar_cities', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Users by cities'
        },
        subtitle: {
            text: 'become'
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Count (users)'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'Count: <b>{point.y:.1f} users.</b>'
        },
        series: [{
            name: 'UsersByCity',
            data: usersByCity,
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });

}
function drawAges(statByAge) {
    Highcharts.chart('pie_chart', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Users By Their Age'
        },
        tooltip: {
            pointFormat: 'Age: {point.name} - <b>{point.percentage:.1f}%</b>'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Ages',
            colorByPoint: true,
            data: statByAge
        }]
    });
}
function drawCountries(usersByCountry) {
    Highcharts.chart('bar_countries', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Users by countries'
        },
        subtitle: {
            text: 'become'
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Count (users)'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'Count: <b>{point.y:.1f} users.</b>'
        },
        series: [{
            name: 'UsersByCountry',
            data: usersByCountry,
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
}


var BecomeApp = function () {
    this.loading = loading;
};

BecomeApp.prototype.getStatistics = function () {
    const that = this;
    that.toggleLoading(true);
    $.ajax({
        type: 'GET',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: "api/stats",
        success:function(data){
            if(!data || data.usersByAge.length === 0) return;

            let usersByCity = data.usersByCity.map(city =>{
                return [ city.city_name, parseFloat(city.count) ];
            });
            let usersByCountry = data.usersByCountry.map(country =>{
                return [ country.country_name, parseFloat(country.count) ];
            });
            let statByAge = data.usersByAge.map((item, key) =>{
                return {
                    name: item.name,
                    y: parseFloat(item.y),
                    sliced: key===0,
                    selected: key===0
                }});

            drawCities(usersByCity);
            drawAges(statByAge);
            drawCountries(usersByCountry);
            that.toggleLoading(false);
        },
        failure: function (err) {
            that.toggleLoading(false);
            console.log(err);
        }
    });
}

BecomeApp.prototype.getUsers = function (){
    const that = this;
    that.toggleLoading(true);
    $.ajax({
        type: 'GET',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: "api/getUsers",
        success:function(data){
            that.toggleLoading(false);
            $('#users_tbl').DataTable( {
                data: data,
                "columns": [
                    { "data": "id" },
                    { "data": "first_name" },
                    { "data": "last_name" },
                    { "data": "email" },
                    { "data": "birth_date" },
                    { "data": "phone" },
                    { "data": "city_name" }
                ]
            } );
        },
        failure: function (err) {
            that.toggleLoading(false);
            console.log(err);
        }
    });
};

BecomeApp.prototype.fetchRelevantUsers = function(){
    const that = this;
    that.toggleLoading(true);
    $.ajax({
        type: 'GET',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: "api/fetchRelevantUsers",
        success:function(data){
            $('#dups').text(data.duplicates);
            $('#unsaved').text(data.unsaved);
            $('#failed').text(data.failed);
            $('#saved').text(data.saved.length);
            data.saved.forEach(item =>{
                $('#users').append($('<div></div>').text(item.email));
            });
            that.toggleLoading(false);
        },
        failure: function (err) {
            that.toggleLoading(false);
            console.log(err);
        }
    });
}

BecomeApp.prototype.toggleLoading = function(state) {
    this.loading = state;
    document.getElementById('roller_loader').style.visibility = state ? 'visible' : 'hidden';
};

