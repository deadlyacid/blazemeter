<?php
header('Content-Type: text/html');
require_once 'topbar.php';
?>
    <style type="text/css">
        .tg  {width:100%;border-collapse:collapse;border-spacing:0;border-color:#aabcfe;}
        .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aabcfe;color:#669;background-color:#e8edff;}
        .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aabcfe;color:#039;background-color:#b9c9fe;}
        .tg .tg-0pky{border-color:inherit;text-align:left;vertical-align:top}
    </style>
    <div class="my-3 p-3 bg-white rounded box-shadow">

        <div class="container mt-5">
            <div class="row">
                <div class="col-lg-5">
                <table class="tg">
                    <tr>
                        <th class="tg-0pky">Operation</th>
                        <th class="tg-0pky">Status</th>
                    </tr>
                    <tr>
                        <td class="tg-0pky">Duplicates</td>
                        <td class="tg-0pky" id="dups"></td>
                    </tr>
                    <tr>
                        <td class="tg-0pky">Saved / Update</td>
                        <td class="tg-0pky" id="saved"></td>
                    </tr>
                    <tr>
                        <td class="tg-0pky">Unsaved</td>
                        <td class="tg-0pky" id="unsaved"></td>
                    </tr>
                    <tr>
                        <td class="tg-0pky">Failed</td>
                        <td class="tg-0pky" id="failed"></td>
                    </tr>
                </table>

            </div>
        </div>
            <div class="row mt-10">
                <div class="col-lg-7">
                    <div>Details:</div>
                    <div id="users"></div>
                </div>
            </div>
        </div>
    </div>
<script>
    $( document ).ready(function() {
        $( document ).ready(function() {
            let becomeApp = new BecomeApp();
            becomeApp.fetchRelevantUsers();
        });


    });
</script>
<?php
require_once 'footer.php';
