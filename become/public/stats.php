<?php
header('Content-Type: text/html');
require_once 'topbar.php';
?>
    <style type="text/css">
        .tg  {width:100%;border-collapse:collapse;border-spacing:0;border-color:#aabcfe;}
        .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aabcfe;color:#669;background-color:#e8edff;}
        .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aabcfe;color:#039;background-color:#b9c9fe;}
        .tg .tg-0pky{border-color:inherit;text-align:left;vertical-align:top}
    </style>
    <div class="wrapper my-3 p-3 bg-white rounded box-shadow">
        <div class="container mt-5">
            <div class="row">
                <div class="col-lg-5 sm-1">
                    <figure class="highcharts-figure">
                        <div id="pie_chart"></div>
                        <p class="highcharts-description">
                            This pie chart show a segmentation of users by their age.
                        </p>
                    </figure>
                </div>
                <div class="col-lg-5 sm-1">
                    <div id="bar_cities"></div>
                    <p class="highcharts-description">
                        This bar chart show a segmentation of users by their city.
                    </p>
                </div>
                <div class="col-lg-5 sm-1">
                    <div id="bar_countries"></div>
                    <p class="highcharts-description">
                        This bar chart show a segmentation of users by their country.
                    </p>
                </div>
        </div>


        </div>
    </div>
<script>
    $( document ).ready(function() {
        let becomeApp = new BecomeApp();
        becomeApp.getStatistics();
    });
</script>
<?php
require_once 'footer.php';
