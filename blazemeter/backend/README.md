School Task:

Built with php-laravel, mysql

To start simply run:
 
docker-compose up --build

All routes are in routes/api.php
using resources for all necessary Http methods.

Routes are:
===========

POST, GET, PUT, DELETE 

/api/student/:id?

/api/grade/:id?

/api/grade/:id?

==================
ONLY GET:

/api//statistics/student
/api/statistics/course


All Controllers are in app/Http/Controllers/*

Requests with validator of required params



You may save a new student with course attached as well, 
while passing courses param in body (OPTIONAL)


All models are in App\{Course, Grade, Student}

I've tested the saving new courses, students and grades and the statistics results.
Didn't have much time to write test files.



 
