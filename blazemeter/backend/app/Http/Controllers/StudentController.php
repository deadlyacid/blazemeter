<?php

namespace App\Http\Controllers;

use App\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Student[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Http\Response
     */
    public function index()
    {
        return Student::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return void
     */
    public function create(Request $request)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'first_name' => 'required|min:2',
            'last_name' => 'required|min:2',
            'email' => 'required|email:rfc',
            'courses' => 'nullable',
        ],
            [
                'first_name.required'=> 'First Name is Required',
                'last_name.required'=> 'Lirst Name is Required',
                'email.required'=> 'Email is Required',
        ]);

        if(!empty($validatedData)){
            $student = new Student();
            $student->first_name = $validatedData['first_name'];
            $student->last_name = $validatedData['last_name'];
            $student->email = $validatedData['email'];
            $student->save();

            if(isset($validatedData['courses'])) {
                $courses = explode(",", $validatedData['courses']);
                foreach ($courses as $course) {
                    $student->courses()->attach($course);
                }
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        $validatedData = $request->validate([
            'first_name' => 'required|min:2',
            'last_name' => 'required|min:2',
            'email' => 'required|email:rfc',
            'courses' => 'nullable',
        ]);


        $student->fill($request->only(['first_name', 'last_name', 'email']))->save();

        if(isset($validatedData['courses'])) {
            $courses = explode(",", $validatedData['courses']);
            $student->courses()->detach();
            foreach ($courses as $course) {
                $student->courses()->attach($course);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Student $student
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Student $student)
    {
        // remove all courses from this user
        $student->courses()->detach();
        $student->forceDelete();
    }
}
