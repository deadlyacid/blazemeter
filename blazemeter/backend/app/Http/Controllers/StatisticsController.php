<?php

namespace App\Http\Controllers;

use App\Course;
use App\Grade;
use App\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StatisticsController extends Controller
{

    public function student(){
        return Student::withCount(['grades' => function($query){
            $query->select(DB::raw("SUM(grade) as salestotal"));
        }])->get()->first();
    }

    public function course(){
        return Grade::groupBy('course_id')->avg('grade');
        //return Course::all();
    }

}
