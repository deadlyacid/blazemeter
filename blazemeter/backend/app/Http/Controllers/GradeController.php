<?php

namespace App\Http\Controllers;

use App\Course;
use App\Grade;
use App\Student;
use Illuminate\Http\Request;

class GradeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Grade[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Http\Response
     */
    public function index()
    {
        return Grade::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'grade' => 'required',
            'student_id' => 'required',
            'course_id' => 'required',
        ]);

        if(!empty($validatedData)){
            $studentExists = Student::where('id', $validatedData['student_id']);
            if($studentExists->exists()){
                $grade = new Grade();
                $grade->grade = $validatedData['grade'];
                $grade->student_id = $validatedData['student_id'];
                $grade->course_id = $validatedData['course_id'];
                $grade->save();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Grade  $grade
     * @return \Illuminate\Http\Response
     */
    public function show(Grade $grade)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Grade  $grade
     * @return \Illuminate\Http\Response
     */
    public function edit(Grade $grade)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Grade  $grade
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Grade $grade)
    {
        $validatedData = $request->validate([
            'grade' => 'required|min:1',
            'student_id' => 'required',
            'course_id' => 'required'
        ]);

        if (!empty($validatedData)){
            $grade->fill($request->only(['grade', 'student_id', 'course_id']))->save();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Grade  $grade
     * @return \Illuminate\Http\Response
     */
    public function destroy(Grade $grade)
    {
        $grade->forceDelete();
    }
}
