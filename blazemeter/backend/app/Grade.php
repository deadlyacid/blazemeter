<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{

    public function Student(){
        return $this->hasMany(Student::class, 'id', 'student_id');
    }

    public function Course(){
        return $this->hasMany(Course::class, 'id', 'course_id');
    }
}
