<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = ['first_name', 'last_name', 'email'];

    public function courses()
    {
        return $this->belongsToMany('App\Course', 'students_courses');
    }

    public function grades()
    {
        return $this->hasMany('App\Grade', 'student_id', 'id');
    }
}
