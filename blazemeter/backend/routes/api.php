<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resources([
    'student' => 'StudentController',
    'course' => 'CourseController',
    'grade' => 'GradeController',
]);

Route::get('/statistics/student', 'StatisticsController@student');
Route::get('/statistics/course', 'StatisticsController@course');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
