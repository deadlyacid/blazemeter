document.addEventListener("DOMContentLoaded", function(event) {
  let dropArea = document.getElementById('upload-drag-area');
  let delFile = document.getElementById('delete_file');
  let getFile = document.getElementById('get_file');

  dropArea.addEventListener('dragenter', highlight, false);
  dropArea.addEventListener('dragleave', highlight, false);
  dropArea.addEventListener('dragover', highlight, false);
  dropArea.addEventListener('drop', dropFile, false);

  getFile.addEventListener('click', handleOpFile.bind(null, event, 'GET'), false);
  delFile.addEventListener('click', handleOpFile.bind(null, event, 'DELETE'), false);

  function highlight(e) {
    e.preventDefault();
    e.stopPropagation();
    dropArea.classList.add('highlight');
    return false;
  }

  function dropFile(e) {
    e.preventDefault();
    e.stopPropagation();
    let dt = e.dataTransfer;
    let files = dt.files;

    handleFiles(files);
    return false;
  }

  function handleOpFile(evt, operate) {
    let loading = document.getElementById('loader');
    loading.style.display = 'block';
    const fileId = document.getElementById('file_id').value;
    let url = `/${fileId}`;
    fetch(url, {
      method: operate,
    })
      .then((result) => {
        return result.text();

      }).then((data) =>{
        try{
          data = JSON.parse(data);
          if(!data.success){
            alert(data.reason);
            return false;
          }
        }catch (e) {}

        if(operate === 'DELETE' && JSON.parse(data).success){
          alert('File deleted successfully');
          return;
        }
      document.getElementById('minify_result').innerText = data;
      document.getElementById('minify_result').style.display = 'block';
    })
      .catch((err) => {
        console.log(err);
        alert('File doesn\'t exists.')
        document.getElementById('minify_result').style.display = 'none';

      }).finally(()=> loading.style.display = 'none');
  }


});


function handleFiles(files) {
  document.getElementById('minify_result').style.display = 'none';
  document.getElementById('upload-drag-area').classList.remove('highlight');
  let loading = document.getElementById('loader');
  loading.style.display = 'block';
  let file = files[0];
  let url = '/';
  let formData = new FormData();
  formData.append('minifyFile', file);

  fetch(url, {
    method: 'POST',
    body: formData
  })
    .then((result) => {
      return result.text();

    }).then((data) =>{
      try{
        if(!JSON.parse(data).success){
          alert(JSON.parse(data).reason);
          return false;
        }
      }catch (e) {}

    document.getElementById('minify_result').innerText = data;
    document.getElementById('minify_result').style.display = 'block';
  })
    .catch((err) => {
      console.log(err);
      document.getElementById('minify_result').style.display = 'none';

    }).finally(()=> loading.style.display = 'none');
}
