'use strict'
const Helpers = use('Helpers')

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')
const votesModel = use('App/Models/Vote');

Route.get('/gui', ({ view, response }) => {
  response.download(Helpers.resourcesPath('views/index.html'))
});
Route.get('/payment', ({ view, response }) => {
  response.download(Helpers.resourcesPath('views/payment.html'))
});

Route.get('/tests', ({ view, response }) => {
  return response.json({res: 'tests!!'});
}).prefix('/api/');

Route.get('votes', async ({view, response}) => {
  let res = await votesModel.query().fetch();
  return response.json({
    res
  })
})

Route.resource('/', 'MinifyController');
