'use strict'
const Helpers = use('Helpers')
const MinifyService = require('../../Services/MinifyService');
const fs = require('fs');
const mime = require('mime');
//const amqp = require('amqplib/callback_api');


class MinifyController {

  constructor(){
    this.file = null;
    this.minifiedFiles = Helpers.resourcesPath('minified/');
  }

  index({response}){
    return response.json({
        success: true
    });
  }

  async show({ params, response }){
    if(params.id){
      if (!fs.existsSync(`${this.minifiedFiles}${params.id}.js`)) {
        return response.status(400).json({success: false, reason: 'no such id.'})
      }
      const minifyResult = fs.readFileSync(`${this.minifiedFiles}${params.id}.js`);
      return response.status(200)
        .header('X-File-Id', params.id)
        .send(minifyResult);
    }
  }

  async store({ request, response }){
    const uploadRes = await this.getFile(request);
    if(uploadRes.failed){
      return response.status(422).send(uploadRes);
    }
    const tmpFile = Helpers.tmpPath('uploads')+'/'+this.file.fileName;
    return await this.handleUploadedFile(tmpFile, response);
  }

  async update({ params, request, response }){
    const uploadRes = await this.getFile(request);
    if(uploadRes.failed){
      return response.status(422).send(uploadRes);
    }
    const tmpFile = Helpers.tmpPath('uploads')+'/'+this.file.fileName;
    return await this.handleUploadedFile(tmpFile, response, params.id);
  }

  async getFile(request) {
    this.file = request.file('minifyFile', {extnames: ['js'], overwrite: true});
    if(!this.file){
      return {success: false, reason: 'could not get file or missing.', failed: true};
    }
    if(this.file.extname !== 'js'){
      return {success: false, reason: 'only files with .js extension allowed.', failed: true};
    }
    await this.file.move(Helpers.tmpPath('uploads'));
    this.file.moved();
    const mimeType = mime.getType(Helpers.tmpPath('uploads')+ '/' +this.file.fileName);
    if(mimeType === 'application/javascript'){
      return true;
    }
    fs.unlinkSync(Helpers.tmpPath('uploads')+ '/' +this.file.fileName);
    return {success: false, reason: 'mime type not allowed.', failed: true};
  }

  destroy({ params, response }){
    if(params.id){
      try{
        fs.unlinkSync(`${this.minifiedFiles}${params.id}.js`);
        return response.status(200).json({success: true});
      }catch (e) {
        return response.status(400).json({success: false, reason: 'no such id.'})
      }
    }
  }

  async handleUploadedFile(tmpFile, response, specificId) {
    let ms = new MinifyService(specificId);

    let fileId = await ms.requestMinify(tmpFile);
    let isSaveMinify = await ms.saveMinify();
    fs.unlinkSync(tmpFile);
    if(isSaveMinify && fileId){
      return response
        .status(200)
        .header('X-File-Id', fileId)
        .send(ms.minifiedData);
    }
  }
}

module.exports = MinifyController
