const Env = use('Env');
const fs = require('fs');
const Helpers = use('Helpers');
const ClosureCompiler = require('google-closure-compiler').jsCompiler;

class MinifyService{

  constructor(specificId){
    this.minifiedData = '';
    this.generatedId = specificId ? specificId : Date.now();
    this.closureCompiler = new ClosureCompiler({
      compilation_level: 'ADVANCED'
    });
    //this.fileName = `${fileName}.${this.generatedId}.js`;
  }

  requestMinify(tmpFile){
    let that = this;
    let fileToMinify = fs.readFileSync(tmpFile, "utf8");
    fileToMinify = fileToMinify.toString().trim();

    return new Promise((resolve, reject) => {
      that.closureCompiler.run([{
        path: `${Helpers.tmpPath('uploads')}/`,
        src: fileToMinify,
        sourceMap: null // optional input source map
      }], (exitCode, stdOut, stdErr) => {
        if(exitCode !== 0) reject(stdErr);
        that.minifiedData = stdOut[0].src.toString().trim();
        resolve(that.generatedId);
      });
    });
  }

  saveMinify(){
    let that = this;
    const minifiedDest = Helpers.resourcesPath('minified/');
    return new Promise((resolve, reject) =>{
      fs.writeFile(`${minifiedDest}${that.generatedId}.js`,that.minifiedData, (error) =>{
        if (error) reject(error);
        resolve(true);
      });
    })
  }
}
module.exports = MinifyService;
