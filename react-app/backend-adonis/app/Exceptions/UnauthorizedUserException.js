'use strict'

const { LogicalException } = require('@adonisjs/generic-exceptions')

class UnauthorizedUserException extends LogicalException {
  /**
   * Handle this exception by itself
   */
   handle (error, { response }) {
      response.status(401).send('Unauthorized user.');
  }
}

module.exports = UnauthorizedUserException
