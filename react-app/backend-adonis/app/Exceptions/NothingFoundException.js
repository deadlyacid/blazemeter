'use strict'

const { LogicalException } = require('@adonisjs/generic-exceptions')

class NothingFoundException extends LogicalException {
  /**
   * Handle this exception by itself
   */
   handle (error, { response }) {
      response.status(500).send(error.message);
    }
}

module.exports = NothingFoundException
