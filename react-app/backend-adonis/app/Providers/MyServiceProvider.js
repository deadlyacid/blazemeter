const { ServiceProvider } = require('@adonisjs/fold')
const path = require('path');
const fs = require('fs');
const NothingFoundException = require('../Exceptions/NothingFoundException');

class MyServiceProvider extends ServiceProvider{

    register(){
        var that = this;
        var myFactory, myModelRes = null;
        this.app.bind('MyBuilder', async (app) =>{
            const myModel = app.use('App/Models/MyModel');
            const anotherModel = app.use('App/Models/AnotherAuth');
                try{
                    myModelRes = await myModel.query()
                        .where('enabled', 1)
                        .select(['id','db']).orderBy('priority').fetch();
                }

                catch (err){
                    let errMsg = `Error getting from db! ${err}`;
                    console.log(errMsg);
                    //const LogModel = app.use('App/Models/Log');
                    //await LogModel.create({level: 'error', message: errMsg});
                    return false;
                }
                if(!myModelRes || myModelRes.rows.length === 0){
                    let errMsg = 'Error nothing was found!';
                    console.log(errMsg);
                    //const LogModel = app.use('App/Models/Log');
                    //await LogModel.create({level: 'error', message: errMsg});
                    // throw new NothingFoundException(errMsg, 500);
                    return null;
                }


                myFactory = new (require('../Services/MyFactory'))(anotherModel);
                myModelRes.rows.forEach((currSrv)=>{
                    let currSrvClassName = that.capitalizeFirstLetter(currSrv.db);
                    let currSrvClass = path.resolve(__dirname, `../Services/${currSrvClassName}/${currSrvClassName}.js`);
                    let serviceExists = fs.existsSync(currSrvClass);

                    if(serviceExists){
                        myFactory.addServiceToList(currSrv.id, currSrvClassName);
                    }else{
                        (async()=>{
                            let errMsg = `Error! ${currSrvClassName} class not found`;
                            console.log(errMsg);
                            //const LogModel = app.use('App/Models/Log');
                            //await LogModel.create({level: 'error', message: errMsg});
                            return;
                        })();
                    }
                });
                return await myFactory.makeIOC();
        });
    }

    capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
}

module.exports = MyServiceProvider;
