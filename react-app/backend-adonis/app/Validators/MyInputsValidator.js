'use strict'

class MyInputsValidator {
    get rules () {
        return {
            phone_number: 'required_without_all:email_address',
        }
    }

    get messages (){
        return {
            required: '{{ field }} is required',
            required_without_all: '{{ field }} param is missing and its required'
        }
    }
}

module.exports = MyInputsValidator;
