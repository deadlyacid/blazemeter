'use strict'
const UnauthorizedUser = use('App/Exceptions/UnauthorizedUserException')

class MyAuth {
  async handle ({ request }, next) {
    const Env = use('Env');
    let secretToken = Env.get('SECRET_TOKEN');

    let inputToken = request.input('key') || request.get('key');
    if(inputToken === secretToken){
        await next()
    }else{
      throw new UnauthorizedUser();
    }

  }
}

module.exports = MyAuth
