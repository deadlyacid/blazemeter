'use strict'

const Model = use('Model')

class ModelAuth extends Model {

    static get table () {
        return 'model_auth';
    }

    static get updatedAtColumn () {
      return null;
    }

    Related(){
        return this.hasOne('App/Models/RelateTable', 'this_key', 'foreign_key')
    }


    static scopeGetDataById(query, id){
        return query.where('tble_id', id)
            .where('enabled', 1)
            .where('environment', '-1')
            .with('Related')
            .orderBy('timestamp', 'ASC')
            .limit(1);
    }
}

module.exports = ModelAuth;
