let callerSite = require('callsite');
const Config = use('Config');

class ResponseAdapterBase{

    constructor(){
        this.uuid = Config.get('flowUuid', null);
        this.rawResponse = {};
        this.parsedResponse = {};
        this.rawData = {};
        this.runningSrvList = [];
    }

    addResponse(response){
        let srvName = callerSite()[1].getFileName().match(/[^\\\/]+(?=\.[\w]+$)|[^\\\/]+$/)[0];
        this.rawResponse[srvName] = [];
        this.rawResponse[srvName].push(response);
        let indx = this.runningSrvList.indexOf(srvName.toLowerCase());
        this.runningSrvList.splice(indx, 1);
    }

    addParsedResponse(newResponse){
        newResponse = newResponse.parsedResponse || [];
    }

    onError(errMsg){
        return {
            id: this.uuid,
            success: false,
            errMessage: errMsg
        }
    }

    onResult(form){
        let returnResult = {
            success: true,
            form
        };
        this.parsedResponse = [];
        Config.set('flowUuid', null);
        return returnResult;
    }
}

module.exports = ResponseAdapterBase;
