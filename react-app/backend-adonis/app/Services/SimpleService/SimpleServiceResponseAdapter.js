
class SimpleServiceResponseAdapter{

    constructor(sServiceResponse){
        this.sServiceResponse = sServiceResponse;
        this.parseResponse();
    }

    parseResponse(){
        if (this.sServiceResponse.length === 0) return {};

        let res = this.sServiceResponse.data[0];
        this.parsedResponse = {
            key: res['val']
        };
    }

}


module.exports = SimpleServiceResponseAdapter;
