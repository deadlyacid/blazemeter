const request = require('request');
const BaseCrawler = require('../BaseCrawler');
const SimpleServiceResponseAdapter = require('./SimpleServiceResponseAdapter');


class SimpleService extends BaseCrawler {
    constructor(srvDataObj) {
        super(srvDataObj);
        this.requestTO = request.defaults();
    }

    async start(params) {
        var result = {};
        this.params = params;

        try {
            result = await this.sendApiRequest();
        } catch (error) {
            logger.error(`API Error: ${error}`);
            return result;
        }

        this.responseAdapter.addResponse(result.data);
        this.responseAdapter.addParsedResponse(new SimpleServiceResponseAdapter(result));
        return true;
    };

    sendApiRequest() {
        let apiUrl = '';
        if(!apiUrl) return;
        var options = {
            url: apiUrl,
            headers: {
                'authorization'         : `Bearer ${this.username}`,
                'grpc-accept-encoding'  : 'gzip',
                'content-type'          : 'application/json',
                'Content-Length'        : '0',
            },
            timeout: 10000
        };

        return new Promise((resolve, reject) => {
            this.requestTO(options, (error, response, body) => {
                "use strict";
                if (!error && response.statusCode === 200) {
                    let res = JSON.parse(body);
                    resolve(res);
                } else {
                    reject(error);
                }
            });
        })
    }

}

module.exports = SimpleService;




