
class BaseServices{
    constructor(serviceDataObj) {
        this.serviceDataObj = serviceDataObj;
        this.responseAdapter = serviceDataObj.responseAdapter;
        this.setCredentials();
        this.setAPIEndpoint();
    }

    setCredentials() {
        let username = this.serviceDataObj.username,
            password = this.serviceDataObj.password;
        if(!this.isEmpty(username)){
            this.username = username;
            this.password = password;
        }else{
            console.error('Could not set credentials, missing param.');
        }
    }

    setAPIEndpoint() {
        this.endpoint = this.serviceDataObj.api_endpoint;
    }

    isEmpty(val) {
        return (val === undefined || val === null || val.length <= 0);
    }
}

module.exports = BaseServices;
