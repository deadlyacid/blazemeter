const ResponseAdapterBase = use('ResponseAdapterBase');
const logger = use('Logger');

class MyFactory{

    constructor(TheModel ){
        this.TheModel = TheModel;
        this.responseAdapter = new ResponseAdapterBase();
        this.allSrvMap = [];
    }

    addServiceToList(id, srvClass){
        this.allSrvMap.push({id: id, srvClass: srvClass});
        this.responseAdapter.runningSrvList.push(srvClass.toLowerCase());
    }

    makeIOC(){
        var that = this;
        let indx = 0;
        that.myServices = [];
        return new Promise((resolve, reject) => {
            that.allSrvMap.forEach(async(currSrv, key) => {
                let id = currSrv.id;
                let srvName = currSrv.srvClass;
                let srvData = await that.TheModel.query().getDataById(id).fetch();

                srvData = srvData.toJSON();

                if(!srvData || srvData.length === 0) {
                    let errMsg = `${srvName} has no data in DB!`;
                    logger.error(errMsg);
                    if(Object.keys(that.allSrvMap).length-1 === indx){
                        reject(errMsg);
                        return;
                    }
                }

                srvData[0].responseAdapter = that.responseAdapter;
                let srvInstance = new (require(`./${srvName}/${srvName}`))(srvData[0]);
                that.myServices[indx] = srvInstance;

                if(Object.keys(that.allSrvMap).length-1 === indx){
                    resolve({
                        services: that.myServices,
                        responseAdapter: that.responseAdapter
                    });
                }
                indx++;
            });
        });
    }
}


module.exports = MyFactory;
