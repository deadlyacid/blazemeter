
Stream = require('node-rtsp-stream');

class RtspService {

    constructor() {
        this.url = null;
        this.stream = null;
    }

    startStream(url) {
        this.url = url;
        let name = url.split("/")[1];
        if(!this.url || !(~this.url.indexOf('rtsp://'))) return false;
        console.log(1);
        if (!this.stream) {
            console.log(2, this.stream);
            try {
                this.stream = new Stream({
                    name: name,
                    streamUrl: this.url,
                    wsPort: 9999,
                    ffmpegOptions: { // options ffmpeg flags
                        '-stats': '', // an option with no neccessary value uses a blank string
                        '-r': 30 // options with required values specify the value after the key
                    }
                });
                this.stream.on('exitWithError', () => {
                    this.stream.stop();
                });

                this.stream.on('EADDRINUSE', () => {

                });

                this.stream.wsServer.on('error', (err) =>{
                    console.log('Stoping..');
                    this.stream.stop();
                    this.stream = null;
                    //this.startStream(url);
                });

            } catch (e) {
                console.log(e);
                this.stream.stop();
            }
        } else {
            this.stream.streamUrl = this.url;
        }
    }
}

module.exports = RtspService;

