const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const User = require('./models/User');
const Rtsp = require('./models/Rtsp');
const jwt = require('jsonwebtoken');
const cookieParser = require('cookie-parser');
const appAuth = require('./middleware/auth');
const cors = require('cors')
const app = express(cookieParser);
const secret = '54dsf54ew1r213254erwd1254eqw2d121';
const RtspService = require('./services/rtsp-service');


mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGODB_URI || `mongodb://localhost:27017/my-app-db`);

app.options('*', cors({credentials: true, origin: true, exposedHeaders: ['x-access-token']}))
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-access-token");
    next();
})
app.use(cors({credentials: true, origin: true, exposedHeaders: ['x-access-token']}), bodyParser.json());

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
    console.log(`app running on port ${PORT}`)
});

app.get('/checkAppToken', appAuth, function(req, res) {
    res.sendStatus(200);
});

app.get('/api/rtsp/:urlId?', appAuth, function(req, res) {
    let findBy = { email: req.email };
    let specificId = req.params.urlId;
    if( specificId ){
        findBy['_id'] = specificId;
    }
    Rtsp.find(findBy, (err, data) =>{
        if(err){
            res.sendStatus(500);
        }
        // if user want to watch specific stream
        if(specificId){
            //streamSrv.stream(data[0].link);
            const link = data[0].link;
            let streamSrv = new RtspService();
            streamSrv.startStream(link);
        }
        res.status(200).json(data);
    });
});

app.post('/api/rtsp', appAuth, function(req, res) {
    const email = req.email;
    const link = req.body.rtspLink;
    const rtsp = new Rtsp({ email, link });
    rtsp.save((err) =>{
        if(err){
            res.status(500).send('Error saving data');
        } else{
            res.status(200).json(rtsp);
        }
    })
});

// Register new user
app.post('/api/register', function(req, res) {
    const { email, password, name } = req.body;
    const user = new User({ email, password, name });
    user.save(function(err) {
        if (err) {
            res.status(500)
                .send("Error registering new user please try again. " + JSON.stringify(err));
        } else {
            res.status(200).json({ email, name });
        }
    });
});

// Login user
app.post('/api/login', function(req, res) {
    const { email, password } = req.body;
    User.findOne({ email }, function(err, user) {
        if (err) {
            console.error(err);
            res.status(500)
                .json({
                    error: 'Internal error please try again'
                });
        } else if (!user) {
            res.status(401)
                .json({
                    error: 'Incorrect email or password'
                });
        } else {
            user.isPasswordMatch(password, function(err, matched) {
                if (err) {
                    res.status(500)
                        .json({
                            error: 'Internal error please try again'
                        });
                } else if (!matched) {
                    res.status(401)
                        .json({
                            error: 'Incorrect email or password'
                        });
                } else {
                    const payload = { email };
                    const token = jwt.sign(payload, secret, {
                        expiresIn: '1h'
                    });
                    const userFullName = user.name;
                    res.header('Access-Control-Allow-Credentials', true);
                    res.cookie('token', token, { httpOnly: true })
                        .status(200).json({ email, userFullName , token});
                }
            });
        }
    });
});
