import React, { Component } from 'react';

export default class Register extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email : '',
            password: '',
            name: ''
        };
    }

    handleInputChange = (event) => {
        const { value, name } = event.target;
        this.setState({
            [name]: value
        });
    }

    onSubmit = (event) => {
        event.preventDefault();
        fetch('http://localhost:5000/api/register', {
            method: 'POST',
            body: JSON.stringify(this.state),
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(res => {
                if (res.status === 200) {
                    this.props.history.push('/');
                } else {
                    const error = new Error(res.error);
                    throw error;
                }
            })
            .catch(err => {
                console.error(err);
                alert('Error logging in please try again');
            });
    }

    render() {
        return (
            <form onSubmit={this.onSubmit}>
                <h3>Sign Up</h3>

                <div className="form-group">
                    <label>Full name</label>
                    <input type="text" className="form-control"
                           name="name"
                           value={this.state.name}
                           onChange={this.handleInputChange}
                           required placeholder="Full name" />
                </div>


                <div className="form-group">
                    <label>Email address</label>
                    <input type="email" className="form-control"
                           name="email"
                           value={this.state.email}
                           onChange={this.handleInputChange}
                           required placeholder="Enter email" />
                </div>

                <div className="form-group">
                    <label>Password</label>
                    <input type="password" className="form-control"
                           name="password"
                           value={this.state.password}
                           onChange={this.handleInputChange}
                           placeholder="Enter password" />
                </div>

                <button type="submit" className="btn btn-primary btn-block">Sign Up</button>
                <p className="forgot-password text-right">
                    Already registered <a href="#">sign in?</a>
                </p>
            </form>

        );
    }
}
