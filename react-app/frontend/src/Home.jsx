import PropTypes from 'prop-types'
import React, { Component } from 'react';
//import { useHistory } from 'react-router-dom'

export default class Home extends Component {
    /*static contextTypes = {
        router: PropTypes.object
    };*/

    constructor(props) {
        super(props);
        this.props = props;
        //this.myHistory = useHistory();
        this.state = {
            rtspLink: '',
            redirect: false
        }
    }
/*
    renderRedirect = () => {
        if (this.state.redirect) {
            return <Redirect to='/player' />
        }
    }*/

    handleInputChange = (event) => {
        const { value, name } = event.target;
        this.setState({
            [name]: value
        });
    }

    onSubmit = (event) => {
        const token = localStorage.getItem('token');
        const myHeaders = new Headers({
            'Content-Type': 'application/json',
            'x-access-token': token
        });

        event.preventDefault();
        fetch('http://localhost:5000/api/rtsp', {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify(this.state),
            headers: myHeaders
        })
            .then(res => {
                if (res.status === 200) {
                    return res.json();
                } else {
                    const error = new Error(res.error);
                    throw error;
                }
            }).then(res =>{
            if(res.link){
                //this.myHistory.push(`/player/${res._id}`);
                this.props.history.push(`/player/${res._id}`);
            }
        })
            .catch(err => {
                console.error(err);
                alert('Error please try again');
            });
    }

    render() {
        return (
            <form onSubmit={this.onSubmit}>
                <div>
                <h1>RTSP Link</h1>
                <p>Put your rtsp-url bellow</p>
                    <input type="text" placeholder="rtsp://test.sww/23"
                           value={this.state.rtspLink}
                           className="form-control"
                           onChange={this.handleInputChange}
                           required name="rtspLink"/>
                </div>
                <button type="submit" className="btn btn-primary btn-block mt-3">Submit</button>
            </form>
        );
    }
}
