import React, { Component } from 'react';
import { BrowserRouter as Router,Link, Route, Switch } from 'react-router-dom';
import Home from './Home';
import Login from './Login';
import Logout from './Logout';
import Register from './Register';
import Player from './Player';
import authGuard from "./authGuard";
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';

import './App.css';

export default class App extends Component {
    componentDidUpdate(prevProps) {
        /*const isLoggingOut = prevProps.isLoggedIn && !this.props.isLoggedIn
        const isLoggingIn = !prevProps.isLoggedIn && this.props.isLoggedIn

        if (isLoggingIn) {
            dispatch(navigateTo(redirectUrl))
        } else if (isLoggingOut) {
            // do any kind of cleanup or post-logout redirection here
        }*/
    }

    newLinkHandle(newLink) {
        this.setState({newLink: newLink});
    }

    render() {
        return (
            <Router>
            <div className="App">
                <nav className="navbar navbar-expand-lg navbar-light fixed-top">
                    <div className="container">
                        <Link className="navbar-brand" to={"/"}>Home</Link>
                        <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
                            <ul className="navbar-nav ml-auto">
                                <li className="nav-item">
                                    <Link className="nav-link" to={"/player"}>Player</Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link" to={"/login-page"}>Login</Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link" to={"/register-page"}>Register</Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link" to={"/logout-page"}>Logout</Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>

                <div className="auth-wrapper">
                    <div className="auth-inner">
                        <Switch>
                            <Route path="/" exact component={authGuard(Home)} />
                            <Route path="/login-page" component={Login} />
                            <Route path="/register-page" component={Register} />
                            <Route path="/logout-page" component={Logout} />
                            <Route path="/player/:url?" component={authGuard(Player)} />
                        </Switch>
                    </div>
                </div>
            </div>
        </Router>
    );
    }
}

