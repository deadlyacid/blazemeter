import React, { Component } from 'react';
const JSMpeg = require('jsmpeg-player');

export default class Player extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            currentUrl: null,
            links: []
        };
        if (props.match.params.url){
            const urlId = props.match.params.url;
            this.fetchItems(urlId)
        }
        this.fetchItems();
        this.handlePickLink = this.handlePickLink.bind(this);
    }

    fetchItems(specificId) {
        const token = localStorage.getItem('token');
        const myHeaders = new Headers({
            method: 'GET',
            'Content-Type': 'application/json',
            'x-access-token': token
        });
        fetch(`http://localhost:5000/api/rtsp/${specificId ? specificId : ''}`,{
            headers: myHeaders
        })
            .then(res => {
                if (res.status === 200) {
                    return res.json();
                } else {
                    const error = new Error(res.error);
                    throw error;
                }
            }).then (res =>{
                if (specificId){
                    this.setState((state) => ({currentUrl: res}));
                } else{
                    this.setState((state) => ({links: res}));
                }
                this.setState({ loading: false });
        })
            .catch(err => {
                console.error(err);
                this.setState({ loading: false, redirect: true });
            });
    }

    handlePickLink = (e) => {
        e.preventDefault();
        if (e.target.dataset && e.target.dataset.item){
            if (this.player){
                this.player.destroy();
                let videoContainer = document.getElementById('rtsp_video');
                let canvas = document.createElement('canvas');
                canvas.id = 'canvas';
                videoContainer.appendChild(canvas);
            }
            this.fetchItems(e.target.dataset.item);
        }
    }

    render() {
        const { links, loading } = this.state;
        if (loading) {
            return null;
        }

        const urlList = this.state.links.map((item, key) =>{
            return (<li key={key}><a href="#" onClick={this.handlePickLink} key={key} data-item={item['_id']}>{item['link']} </a></li>)
        });

        if(this.state.currentUrl){
            this.player = new JSMpeg.Player('ws://localhost:9999', {
                canvas: document.getElementById('canvas')
            });
        }
        return (
            <div className="container">
                <div className="row rtsp-list">
                    <ul>{urlList}</ul>
                </div>
                <div id="rtsp_video" className="row mt-4 rtsp-video">
                    <canvas id="canvas"></canvas>
                </div>
            </div>

        );
    }
}
