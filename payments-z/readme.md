Zeitgold-Payments
=================

This app built in nodejs using express-js framework.

How to run it:

`npm install`

`npm run start`

App's Structure:
----------------

This web-app expose one endpoint "/link" on POST request, which defined inside routes folder `zeit-g.js`
The request is being handle by the controller (`paymatch-controller.js`),
There you may find a call for Conditions class which in charge handle all of the conditions that may be used by the system.
e.g: reference-match, date condition, amount etc..

As a setup for this exercise I've thought about two course of actions:
 - First we try to find a match by referenceId, So inside Conditions class we add the conditions we need for this course of action.
    - Reference
    - Date
    - Amount
    
 - Second we try to find a match by month (standard declared as 1 month between payable-payment).
 
 
The `Conditions` class inherits from `ConditionAbstract` that holds the Conditions[] classes.

Each `Condition` class implements `validate` & `onError` methods so after we've initializes all of them
We can access those methods. 

After adding all of conditions we need, we trigger the validateAll() method
Which trigger the validate() method inside each condition class.
By using this structure it will be easy to add/remove condition in the future.

When first process is finished, we check if the amount of the payment is bigger
 than the amount of our matching result.
 
 If not we can assume that there are more options we have to look for.
 Otherwise user will get the answer as expected.
 


