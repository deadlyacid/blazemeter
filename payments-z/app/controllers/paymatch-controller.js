const payables2 = require('../services/entities/payable');
const Conditions = require('../conditions/conditions');

class PayMatchController {

    constructor() {
        this.findMatch = this.findMatch.bind(this);
    }

    findMatch(req, res){
        const paymentsInput = req.body;
        if(!paymentsInput || Object.keys(paymentsInput).length === 0) return res.status(400).send('Missing Params');
        if(Array.isArray(paymentsInput)){
            // handle case of multiple payments input - not relevant right now.
        }else{
            let resultRef = {count: 0, sumAmount: 0};
            let resultMonth = {count : 0};
            let refCondition = new Conditions(payables2, paymentsInput);
            resultRef = refCondition.validateByRef();
            if(paymentsInput.amount > resultRef.sumAmount){
                resultMonth = refCondition.validateByMonth(resultRef['alreadyMatched']);
            }

            const payableNum = `Expected number of payables: ${resultRef.count + resultMonth.count}`;
            res.status(200).send(payableNum);
        }




    }
}
module.exports = new PayMatchController();

