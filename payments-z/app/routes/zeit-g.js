const express = require('express');
const router = express.Router();
const PayMatchController  = require('../controllers/paymatch-controller');

router.route('/')
    .post(PayMatchController.findMatch);
module.exports = router;

