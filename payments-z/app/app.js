const express = require('express');
const app = express();
const port = 3000;

const zeit_g = require('./routes/zeit-g');
const router = express.Router();

app.use(express.json());
app.use('/link', zeit_g);

app.listen(port, () => console.log(`App listening on port ${port}!`));


