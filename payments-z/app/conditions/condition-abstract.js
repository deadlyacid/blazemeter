const _ = require('lodash');

class ConditionAbstract{

    constructor(payableObj, paymentInput) {
        this.payment = paymentInput;
        this.payableObj = payableObj;
        this.result = [];
        this.paymentReference = paymentInput.payment_reference.split(" ");
        this.conditions = [];
    }

    validateByRef(){}

    validateByMonth() {}

    addCondition(condition){
        this.conditions.push(condition);
    }

    validateAll(forceFilter){
        if(forceFilter) this.result = forceFilter;
        try{
            let sumAmount = 0;
            this.conditions.forEach(currCondition => {
                if (!currCondition.validate(this.result)){
                    throw new Error(JSON.stringify(currCondition.onError()));
                }
                this.result = currCondition.result;
                sumAmount += currCondition['sum'] ? currCondition['sum'] : sumAmount;
            });

            this.conditions = [];
            if(forceFilter){
                this.result = [_.sortBy(this.result, ['amount'])[this.result.length-1]];
            }
            return {
                success: true,
                sumAmount: sumAmount,
                count: this.result.length,
                alreadyMatched: this.result
            }
        } catch(e) {
            this.conditions = [];
            return {
                success: false,
                count: 0,
                sumAmount: 0,
                alreadyMatched: []
            }
        }
    }
}

module.exports = ConditionAbstract;
