const Reference = require('./reference');
const Date = require('./date');
const Amount = require('./amount');
const _ = require('lodash');
const ConditionAbstract = require('./condition-abstract')

class Conditions extends ConditionAbstract{

    constructor(payableObj, paymentInput){
        super(payableObj, paymentInput);
    }

    validateByRef(){
        this.addCondition(new Reference(this.payableObj.Payables2, this.paymentReference));
        this.addCondition(new Date(this.payableObj.Payables2, this.payment.payment_date));
        this.addCondition(new Amount(this.payableObj.Payables2, this.payment.amount));
        return this.validateAll();
    }

    validateByMonth(alreadyMatched){
        const filterMatched = alreadyMatched.length>0 ? this.payableObj.Payables2.filter(function(payableItem) {
            return !!alreadyMatched.find(function(matchedItem) {
                return payableItem.referenceId !== matchedItem.referenceId
            })
        }) : this.payableObj.Payables2;

        this.addCondition(new Date(filterMatched, this.payment.payment_date));
        return this.validateAll(filterMatched);
    }
}

module.exports = Conditions;
