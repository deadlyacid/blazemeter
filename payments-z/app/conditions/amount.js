const _ = require('lodash');
const moment = require('moment');

class Amount {

    constructor(payableObj, paymentAmount) {
        this.result = null;
        this.sum = 0;
        this.payableObj = payableObj;
        this.paymentAmount = paymentAmount;
    }

    validate(filteredPayables){
        this.result = filteredPayables;
        filteredPayables.forEach(currPayable => {
            this.sum += currPayable.amount;
        });
        return this.paymentAmount >= this.sum;
    }

    onError(){
        this.sum = 0;
        return {
            code: 3,
            message: 'Could not match by amount.'
        }
    }
}

module.exports = Amount;
