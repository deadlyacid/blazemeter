const _ = require('lodash');

class Reference {

    constructor(payableObj, paymentReference) {
        this.result = null;
        this.payableObj = payableObj;
        this.paymentReference = paymentReference;
    }

    validate(){
        this.result = _(this.payableObj).keyBy('referenceId').at(this.paymentReference).value();
        return this.result !== null && Object.keys(this.result).length > 0;
    }

    onError(){
        return {
            code: 1,
            message: 'Could not match by reference id.'
        }
    }
}

module.exports = Reference;
