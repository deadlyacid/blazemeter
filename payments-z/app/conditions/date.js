const _ = require('lodash');
const moment = require('moment');

class Date {

    constructor(payableObj, paymentDate) {
        this.result = null;
        this.payableObj = payableObj;
        this.paymentDate = paymentDate;
    }

    validate(filteredPayables){
        this.result = filteredPayables.filter(currPayable => {
            let timeDiffM = moment(this.paymentDate).diff(moment(currPayable.dateOccurred), 'month');
            let timeDiffD = moment(this.paymentDate).diff(moment(currPayable.dateOccurred), 'day');
            return timeDiffD >=0 && timeDiffM <=1;
        });
        return this.result !== null && Object.keys(this.result).length > 0;
    }

    onError(){
        return {
            code: 2,
            message: 'Could not match by date occurred.'
        }
    }
}

module.exports = Date;
